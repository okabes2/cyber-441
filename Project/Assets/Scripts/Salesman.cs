﻿using System;
using Random = UnityEngine.Random;

[Serializable]
public class Salesman
{
    // Initial physical fitness
    public float defaultStamina = 100;
    // Parameters for probability adjustment
    public float rate = 1f;
    // Physical fitness reduction rate
    public float decreaseRate = 0.98f;
    // When it falls below this value, the patrol ends.
    public float finalStamina = 1.0e-7f;
    // Current physical fitness
    public float stamina;
 
    public Salesman()
    {
        stamina = defaultStamina;
    }

    /// <summary>
    /// Whether to select even if the distance is long
    /// </summary>
    public bool Judge()
    {
        float rot = Random.Range(0, defaultStamina);
        return rot < stamina * rate;
    }

    /// <summary>
    ///Reduce physical strength
    /// </summary>
    public void Travel() 
    {
        stamina *= decreaseRate;
    }

    /// <summary>
    /// Physical fitness reset
    /// </summary>
    public void Reset()
    {
        stamina = defaultStamina;
    }
}