﻿using System.Collections.Generic;
using UnityEngine;

public class PrefectureData
{
    public readonly int id;
    // 県庁所在地名(Prefectural office location name)
    public readonly string name;
    // 経度(longitude)
    public readonly double longitude;
    // 緯度(latitude)
    public readonly double latitude;

    // Unity上のオブジェクト(Objects on Unity)
    public GameObject ownObj;

    public PrefectureData(List<string> csvLine)
    {
        id = int.Parse(csvLine[0]);
        name = csvLine[1];
        latitude = double.Parse(csvLine[2]);
        longitude = double.Parse(csvLine[3]);
    }
}
