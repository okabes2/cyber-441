﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PrefectureMap : MonoBehaviour
{
    private const string POINT_PREFAB_PATH = "Prefabs/point";
    
    [SerializeField] private string _csvPath = "Csv/pref";
    [SerializeField] private Salesman _salesman;
    [SerializeField] private Text _distanceText;

    private List<PrefectureData> _prefectureList;
    private List<Vector3> _pointList = new List<Vector3>();
    
    private void Start()
    {
        _prefectureList = CreateMap();
        (var route, double distance) = SolveTsp(_prefectureList);
        MakePointList(route);
        _distanceText.text = $"Distance:{(distance/1000f):F3}km";
    }

    public void OnClickRetry()
    {
        (var route, double distance) = SolveTsp(_prefectureList);
        MakePointList(route);
        _distanceText.text = $"Distance:{(distance/1000f):F3}km";
    }
    
    static Material lineMaterial;
    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    private void OnRenderObject()
    {
        CreateLineMaterial();
        lineMaterial.SetPass(0);

        GL.PushMatrix();
        GL.MultMatrix(transform.localToWorldMatrix);
        GL.Begin(GL.LINE_STRIP);
        GL.Color(new Color(0,1,1));
        foreach (var point in _pointList)
        {
            GL.Vertex(point);
        }
        GL.End();
        GL.PopMatrix();
    }

    private List<PrefectureData> CreateMap()
    {
        var csv = CsvLoader.Load(_csvPath);
        var pointPrefab = Resources.Load<GameObject>(POINT_PREFAB_PATH);

        var ret = new List<PrefectureData>();
        csv.ForEach(itr =>
        {
            var data = new PrefectureData(itr);
            ret.Add(data);
            data.ownObj = Instantiate(pointPrefab, this.transform);
            data.ownObj.name = data.name;
            data.ownObj.transform.localPosition = new Vector3((float)data.longitude, (float)data.latitude);
        });

        return ret;
    }

    private void MakePointList(List<PrefectureData> prefectureList)
    {
        _pointList.Clear();

        foreach (var data in prefectureList) {
            _pointList.Add(data.ownObj.transform.position);
        }
    }

    /// <summary>
    /// Find a route close to the shortest route(最短経路に近い経路を求める)
    /// </summary>
    /// <param name="prefectureList">Prefectural office location data(県庁所在地データ)</param>
    /// <returns>route:(経路) distance:(Total distance 総距離</returns>
    private (List<PrefectureData> route, double distance) SolveTsp(List<PrefectureData> prefectureList)
    {
        _salesman.Reset();
        
        // route ルート
        List<PrefectureData> optimalRoute = null;
        double optimalDistance = 0f;

        int loopCount = 0;

        while (_salesman.stamina > _salesman.finalStamina) {

            // Current prefecture(area) 現在居る県
            var currentPref = prefectureList[Random.Range(0, prefectureList.Count)];

            // Start prefecture(スタート県)
            var startPref = currentPref;
            // Prefecture to be visited(巡回予定の県)
            var targetPrefs = prefectureList.ToList();
            targetPrefs.Remove(startPref);

            // The prefecture you searched for last time(前回検索した県)
            PrefectureData prevPref = null;
            // Save last distance(前回の距離を保存)
            double prevDistance = 0f;
            // Undetermined prefecture(未判定の県)
            var undecidedPrefs = targetPrefs.ToList();

            // Current route(現在のルート)
            List<PrefectureData> currentRoute = new List<PrefectureData>();
            currentRoute.Add(startPref);

            // Total travel distance(総移動距離)
            double totalDistance = 0f;

            while (true) {
                loopCount++;
                var nextPref = undecidedPrefs[Random.Range(0, undecidedPrefs.Count)];

                // Distance to the next point(次の地点の距離)
                double nextDistance = GeoUtil.GeoDistance(nextPref.latitude, nextPref.longitude, currentPref.latitude,
                    currentPref.longitude, 10);

                // Deleted from judgment target(判定対象から削除)
                undecidedPrefs.Remove(nextPref);

                // When there is no judgment target(判定対象がない場合)
                if (prevPref == null) {
                    if (undecidedPrefs.Count <= 0) {
                        // End of patrol(巡回終了)
                        currentRoute.Add(nextPref);
                        totalDistance += nextDistance;
                        currentRoute.Add(startPref);
                        totalDistance += GeoUtil.GeoDistance(nextPref.latitude, nextPref.longitude, startPref.latitude,
                            startPref.longitude, 10);
                        break;
                    }
                    prevPref = nextPref;
                    prevDistance = nextDistance;
                    continue;
                }

                // Judgment of distance between two points(二つの地点の距離判定)
                if (nextDistance < prevDistance) {
                    // The distance is shorter than the one judged last time(前回判定したものよりも距離が短い)
                    prevPref = nextPref;
                    prevDistance = nextDistance;
                } else if (_salesman.Judge()) {
                    // Replace even if the distance is long(距離が長くても交換する)
                    prevPref = nextPref;
                    prevDistance = nextDistance;
                }

                // Route determination when there is no judgment target(判定対象がない場合は経路決定)
                if (undecidedPrefs.Count <= 0) {
                    currentRoute.Add(prevPref);
                    totalDistance += prevDistance;
                    targetPrefs.Remove(prevPref);
                    undecidedPrefs = targetPrefs.ToList();
                    currentPref = prevPref;
                    prevPref = null;
                }
            }

            // Decreased physical strength(体力減少)
            _salesman.Travel();

            if (optimalRoute == null) {
                // First search
                optimalRoute = currentRoute;
                optimalDistance = totalDistance;
            } else {
                // Select the one with the short total distance from the second time onwards(２回目以降はトータル距離が短いものを選択)
                if (totalDistance < optimalDistance) {
                    optimalRoute = currentRoute;
                    optimalDistance = totalDistance;
                }
            }
        }
        
        Debug.Log($"LoopCount:{loopCount}");
        
        return (optimalRoute, optimalDistance);
    }
}
